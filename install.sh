#!/bin/bash
if [[ "$EUID" -ne 0 ]]; then 
  echo "This script need root"
  exit
fi

echo 'installing plymouth...'
apt-get install -yqq plymouth
echo 'copying files...'
make install
echo 'set plymouth theme...'
plymouth-set-default-theme debian-win10
echo 'generating initramfs...'
update-initramfs -u
echo 'updating grub...'
update-grub
echo 'done.'
